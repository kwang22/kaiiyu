#Jack Castiglione, Oct 14
import numpy as np
from PIL import Image, ImageDraw, ImageFont
import colors


def main():
    #Add a basic image to overlay text onto
    pic = Image.open("images/flowers-wall.jpg").convert("RGBA")

    #Make a blank image for the text, initialized to transparent text color
    draw = Image.new("RGBA", pic.size, colors.transparent)
    #Create a random triangle
    triangle = [(0,0),(0,0),(0,0)]
    for i in range(3):
        triangle[i] = (np.random.randint(0,512), np.random.randint(0,512))

    #Create a drawing context
    d = ImageDraw.Draw(draw)

    #Draw a Polygon
    d.polygon(triangle, colors.white)

    #Overlay the base image and the created layers
    out = Image.alpha_composite(pic, draw)
    out.show()


if __name__ == "__main__":
    main()
